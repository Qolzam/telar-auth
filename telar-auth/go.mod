module gitlab.com/Qolzam/telar-auth/telar-auth

go 1.14

require (
	cloud.google.com/go v0.69.1 // indirect
	github.com/alexellis/hmac v0.0.0-20180624211220-5c52ab81c0de // indirect
	github.com/aws/aws-sdk-go v1.35.9 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-oauth2/oauth2/v4 v4.1.2
	github.com/go-session/session v3.1.2+incompatible
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/golang/snappy v0.0.2 // indirect
	github.com/google/uuid v1.1.2 // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/klauspost/compress v1.11.1 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/openfaas-incubator/go-function-sdk v0.0.0-20200405082418-b31e65bf8a33
	github.com/plivo/plivo-go v5.2.0+incompatible // indirect
	github.com/red-gold/telar-core v0.0.0-20200312120826-3d4f6378c5f6
	github.com/red-gold/telar-web v0.1.0
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/tidwall/buntdb v1.1.2 // indirect
	github.com/tidwall/gjson v1.6.1 // indirect
	go.mongodb.org/mongo-driver v1.4.2 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/net v0.0.0-20201016165138-7b1cca2348c0 // indirect
	golang.org/x/sync v0.0.0-20201008141435-b3e1573b7520 // indirect
	golang.org/x/sys v0.0.0-20201017003518-b09fb700fbb7 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	gopkg.in/oauth2.v3 v3.12.0 // indirect
)

replace github.com/satori/go.uuid => github.com/gofrs/uuid v3.3.0+incompatible
