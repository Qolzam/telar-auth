package function

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-session/session"
	handler "github.com/openfaas-incubator/go-function-sdk"
	tsconfig "github.com/red-gold/telar-core/config"
	tsserver "github.com/red-gold/telar-core/server"
	"github.com/red-gold/telar-core/utils"
	cf "github.com/red-gold/telar-web/src/controllers/users/auth/config"
	service "github.com/red-gold/telar-web/src/services/users"
)

// Login page data template
type loginPageData struct {
	title         string
	orgName       string
	orgAvatar     string
	appName       string
	actionForm    string
	resetPassLink string
	signupLink    string
	message       string
}

func userAuthorizeHandler(w http.ResponseWriter, r *http.Request) (userID string, err error) {
	store, err := session.Start(r.Context(), w, r)
	if err != nil {
		return
	}

	uid, ok := store.Get("LoggedInUserID")
	if !ok {
		if r.Form == nil {
			r.ParseForm()
		}

		store.Set("ReturnUri", r.Form)
		store.Save()

		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}

	userID = uid.(string)
	store.Delete("LoggedInUserID")
	store.Save()
	return
}

// LoginHandler creates a handler for logging in telar social
func LoginHandler(db interface{}) func(http.ResponseWriter, *http.Request, tsserver.Request) (handler.Response, error) {
	coreConfig := &tsconfig.AppConfig
	return func(w http.ResponseWriter, r *http.Request, req tsserver.Request) (handler.Response, error) {

		loginData := &loginPageData{
			title:         "Login - Telar Social",
			orgName:       *coreConfig.OrgName,
			orgAvatar:     *coreConfig.OrgAvatar,
			appName:       *coreConfig.AppName,
			actionForm:    "",
			resetPassLink: "",
			signupLink:    "",
			message:       "",
		}
		store, err := session.Start(r.Context(), w, r)
		if err != nil {
			fmt.Println("[ERROR] Session ", err.Error())
			loginData.message = "Session error!"
			return loginPageResponse(loginData)
		}

		if r.Form == nil {
			if err := r.ParseForm(); err != nil {
				fmt.Println("[ERROR] Form ", err.Error())
				loginData.message = "Form is empty!"
				return loginPageResponse(loginData)
			}
		}
		// Create service
		userAuthService, serviceErr := service.NewUserAuthService(db)
		if serviceErr != nil {
			return handler.Response{StatusCode: http.StatusInternalServerError}, serviceErr
		}

		username := r.Form.Get("username")
		if username == "" {
			fmt.Printf("\n Username is empty\n")
			loginData.message = "Username is required!"
			return loginPageResponse(loginData)
		}
		password := r.Form.Get("password")
		if password == "" {
			fmt.Printf("\n Password is empty\n")
			loginData.message = "Password is required!"
			return loginPageResponse(loginData)
		}
		foundUser, err := userAuthService.FindByUsername(username)
		if err != nil {
			fmt.Printf("\n User not found %s\n", err.Error())
			loginData.message = "User not found!"
			return loginPageResponse(loginData)
		}

		if !foundUser.EmailVerified && !foundUser.PhoneVerified {

			loginData.message = "User is not verified!"
			return loginPageResponse(loginData)
		}
		fmt.Printf("\n foundUser.Password: %s  , model.Password: %s", foundUser.Password, password)
		compareErr := utils.CompareHash(foundUser.Password, []byte(password))
		if compareErr != nil {
			fmt.Printf("\nPassword doesn't match %s\n", compareErr.Error())
			loginData.message = "Password doesn't match!"
			return loginPageResponse(loginData)
		}

		store.Set("LoggedInUserID", username)
		store.Save()
		w.WriteHeader(http.StatusFound)
		return handler.Response{
			StatusCode: http.StatusFound,
			Header: map[string][]string{
				"Location": []string{"/auth"},
			},
		}, nil
	}

}

func authHandler(w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(nil, w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, ok := store.Get("LoggedInUserID"); !ok {
		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}

	outputHTML(w, r, "static/auth.html")
}

func outputHTML(w http.ResponseWriter, req *http.Request, filename string) {
	file, err := os.Open(filename)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer file.Close()
	fi, _ := file.Stat()
	http.ServeContent(w, req, file.Name(), fi.ModTime(), file)
}

// LoginPageHandler creates a handler for logging in
func LoginPageHandler(tsserver.Request) (handler.Response, error) {
	appConfig := tsconfig.AppConfig
	authConfig := &cf.AuthConfig
	prettyURL := utils.GetPrettyURLf(authConfig.BaseRoute)
	loginData := &loginPageData{
		title:         "Login - Telar Social",
		orgName:       *appConfig.OrgName,
		orgAvatar:     *appConfig.OrgAvatar,
		appName:       *appConfig.AppName,
		actionForm:    "",
		resetPassLink: prettyURL + "/password/forget",
		signupLink:    prettyURL + "/signup",
		message:       "",
	}
	return loginPageResponse(loginData)
}

// loginPageResponse login page response template
func loginPageResponse(data *loginPageData) (handler.Response, error) {
	html, parseErr := utils.ParseHtmlBytesTemplate("./html_template/login.html", struct {
		Title         string
		OrgName       string
		OrgAvatar     string
		AppName       string
		ActionForm    string
		ResetPassLink string
		SignupLink    string
		Message       string
	}{
		Title:         data.title,
		OrgName:       data.orgName,
		OrgAvatar:     data.orgAvatar,
		AppName:       data.appName,
		ActionForm:    data.actionForm,
		ResetPassLink: data.resetPassLink,
		SignupLink:    data.signupLink,
		Message:       data.message,
	})
	if parseErr != nil {
		fmt.Printf("Can not parse the html page! error: %s ", parseErr)
		return handler.Response{StatusCode: http.StatusInternalServerError, Body: utils.MarshalError("parseHtmlError", "Can not parse the html page!")},
			nil
	}

	return handler.Response{
		Body:       html,
		StatusCode: http.StatusOK,
		Header: map[string][]string{
			"Content-Type": []string{" text/html; charset=utf-8"},
		},
	}, nil
}
